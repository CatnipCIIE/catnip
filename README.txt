# Catnip's README

# Instalación

1. Instalar pygame

No debería necesitar ninguna librería a mayores pero si no funciona:
  2. Instalar lxml sudo apt-get install python2-lxml
  3. Cambiar el import de Xmlparser por from etree import *

# Como ejecutarlo

Para ejecutar el juego es necesitario hacer un: python main.py

# La memoria se encuentra dentro de la carpeta doc

catnip/doc/ciee_memory.pdf
