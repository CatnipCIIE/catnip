# -*- coding: utf-8 -*-

import pygame, escena
from escena import *
from sprites import *
from XmlParser import *
from pygame.locals import *


# -------------------------------------------------
# -------------------------------------------------
# Constantes
# -------------------------------------------------
# -------------------------------------------------

# Colores para debugear
NEGRO  = (  0,   0,   0)
BLANCO = (255, 255, 255)
VERDE  = (0,   255,   0)
ROJO   = (255,   0,   0)

# Los bordes de la pantalla para hacer scroll horizontal
MINIMO_X_JUGADOR = 10
MAXIMO_X_JUGADOR = ANCHO_PANTALLA - MINIMO_X_JUGADOR -50

MINIMO_Y_JUGADOR = 50
MAXIMO_Y_JUGADOR = ALTO_PANTALLA - MINIMO_Y_JUGADOR

# Lista de opciones en las puntuaciones

SCORE_ENEMIGO = 0
SCORE_BOSS = 1
SCORE_SUSHI = 2
SCORE_TIME = 3
SCORE_DEATH = 4

# -------------------------------------------------
# Clase Fase

class Fase(Escena):



    def __init__(self, director):


            Escena.__init__(self, director)

            #Instanciamos el parser para leer el fichero xml
            parser = XmlParser();
            parser.devolverNivel(director.nivelActual)


            # Creamos el decorado, fondo y variable rara
            self.decorado = Decorado(parser)
            self.pausa = Pausa()
            self.parser = parser
            self.GameOver = GameOver(director)
            self.death = False
            self.score = ScoreTon.getInstance()
            self.timer = 0
            self.iniciate = False

            pygame.mixer.stop()
            self.music = GestorRecursos.load_sound(self.parser.music)
            self.music.play(-1)




            # Creamos los grupos de Sprites
            self.grupoSprites = pygame.sprite.Group()
            self.grupoSpritesDinamicos = pygame.sprite.Group()
            self.grupoSpritesComidas = pygame.sprite.Group()
            self.grupoJugadores = pygame.sprite.Group()
            self.grupoEnemigos = pygame.sprite.Group()
            self.grupoProyectilesAmigo = pygame.sprite.Group()
            self.grupoProyectilesEnemigo = pygame.sprite.Group()
            self.grupoNeutrales = pygame.sprite.Group()
            self.grupoFinFase = pygame.sprite.Group()
            self.ultimaPulsacionPausa= False
            self.boss = None
            self.jugador1 = None


            #self.jugador1 = Jugador(self.grupoSprites, self.grupoSpritesDinamicos, self.grupoJugadores)
            self.invulnerable = 100;

            # Arrays de grupos
            grupoPlataformas = pygame.sprite.Group()
            grupoMuros = pygame.sprite.Group()
            grupoTechos = pygame.sprite.Group()
            self.grupoBloques = [grupoPlataformas, grupoMuros, grupoTechos]


            #self.grupoPersonajes = [self.grupoJugadores, self.grupoEnemigos, self.grupoNeutrales]

            # Creamos lista para guardar todos los sprites




            # Que parte del decorado estamos visualizando
            self.scrollx = 0
            self.scrolly = 0

            # Ponemos a los jugadores en sus posiciones iniciales


            for comid in parser.listaComidas:
                    FactoriaSprite.getFood(comid.tipo, self.grupoSprites, self.grupoSpritesComidas, comid.posicion)


            for per in parser.listaPersonajes:
                    personaje = FactoriaSprite.getPersonaje(per.nombre, self.grupoSprites, self.grupoSpritesDinamicos, self.grupoNeutrales, self.grupoEnemigos, self.grupoJugadores, self.grupoProyectilesAmigo, self.grupoProyectilesEnemigo)
                    personaje.establecerPosicion(per.posicion)

                    if per.nombre == 'Boss':
                        self.boss = personaje
                    if per.nombre == 'Jugador':
                        self.jugador1 = personaje


            for plat in parser.listaPlataformas:
                    FactoriaSprite.getBloque(self.grupoSprites, self.grupoBloques, plat, 0)



            for finFase in parser.listaFinFases:
                FinFase (self.grupoSprites, self.grupoFinFase, finFase.getRect(), 1)




            #inicializar la vida
            if self.boss != None:
                self.vB = vidaBoss (self.boss)
            if self.jugador1 != None:
                self.v=vidaPersonaje(self.jugador1)



        # Devuelve True o False según se ha tenido que desplazar el scroll
    def actualizarScrollOrdenados(self, jugador):
        #print "posxxx: " + str (jugador.posicion[0])
        #print "posxxxIMAG: " + str (self.decorado.imagen.get_rect().right)
   	scrollx_base = (ANCHO_PANTALLA/2)
##new horizontal scroll


	if ((jugador.rect.right+50) >=ANCHO_PANTALLA):
	    if ((self.scrollx+ scrollx_base + ANCHO_PANTALLA) > self.decorado.imagen.get_rect().right):
		#estamos en el caso k scrolleando nos saldremos..hay k ajustar el scroll k haremos

		self.scrollx = self.scrollx + (self.decorado.imagen.get_rect().right- self.scrollx- ANCHO_PANTALLA)
		return True,0
	    else:

            	self.scrollx = self.scrollx + (ANCHO_PANTALLA/2)
            	#print "rect top: " + str(self.scrolly)
            	return True,0

	elif ((jugador.rect.left-50)<=0):
	    if ((self.scrollx -scrollx_base) < 0):
		#estamos en el caso k scrolleando nos saldremos..hay k ajustar el scroll k haremos

		self.scrollx = 0
		return True,0
	    else:

            	self.scrollx = self.scrollx - (ANCHO_PANTALLA/2)
            	return True,0

##new vertical scroll

        if ((jugador.rect.top+50) <= 0):
            #print "entro arriba"
            self.scrolly = self.scrolly - (self.decorado.rect.height/2)
            #print "rect top: " + str(self.scrolly)
            return True,1

        elif ((jugador.rect.bottom)>=ALTO_PANTALLA):
            #print "entro abajo"
            self.scrolly = (self.decorado.rect.top-self.decorado.y) + (self.decorado.rect.height/2)
            return True,1


        return False,0;



    def actualizarScroll(self, jugador):
        # Se ordenan los jugadores según el eje x, y se mira si hay que actualizar el scroll
        cambioScroll,vertical = self.actualizarScrollOrdenados(jugador)

        # Si se cambio el scroll, se desplazan todos los Sprites y el decorado
        if cambioScroll:
            # Actualizamos la posición en pantalla de todos los Sprites según el scroll actual
            for sprite in iter(self.grupoSprites):
                sprite.establecerPosicionPantalla((self.scrollx, self.scrolly))

            # Ademas, actualizamos el decorado para que se muestre una parte distinta

            self.decorado.update(self.scrollx, self.scrolly)





    # Se actualiza el decorado, realizando las siguientes acciones:
    #  Se indica para los personajes no jugadores qué movimiento desean realizar según su IA
    #  Se mueven los sprites dinámicos, todos a la vez
    #  Se comprueba si hay colision entre algun jugador y algun enemigo
    #  Se comprueba si algún jugador ha salido de la pantalla, y se actualiza el scroll en consecuencia
    #     Actualizar el scroll implica tener que desplazar todos los sprites por pantalla
    def update(self, tiempo):
        #vamos frame a frame para restar la pausa
        #if (self.pausa == True):
        #    self.tiempoPausa+=1

        if (self.pausa.getPausa() == False):
            if self.jugador1 != None:

                self.timer += 1

                # No se hace nada con los enemigos hasta que el jugador toque algun boton

                if self.iniciate == True:
                    for enemigo in iter(self.grupoEnemigos):
                        enemigo.mover_cpu(self.jugador1)
                    self.flag_curado = False
                    self.grupoSpritesDinamicos.update(self.grupoBloques, tiempo)



                if pygame.sprite.groupcollide(self.grupoJugadores, self.grupoFinFase, False, False)!={}:

                    self.director.nivelActual +=1
                    self.score.setScore(SCORE_TIME, self.timer)
                    self.director.cambiarEscena (Fase (self.director))

                    #self.director.salirEscena()


                if pygame.sprite.groupcollide(self.grupoJugadores, self.grupoEnemigos, False, False)!={}:
                    obj = pygame.sprite.spritecollideany (self.jugador1, self.grupoEnemigos)
                    if (self.invulnerable > 99 ):
                        print (obj.danoRealizado())
                        print (obj)
                        if ((self.v.getVida()-obj.danoRealizado()) < 0): #muerto
                            self.death = True
                            self.score.setScore(SCORE_DEATH)

                            #self.director.salirEscena()

                        self.v.quitarVida(obj.danoRealizado())
                        self.invulnerable = 0


                if pygame.sprite.spritecollideany (self.jugador1, self.grupoSpritesComidas)!=None :
                    obj = pygame.sprite.spritecollideany (self.jugador1, self.grupoSpritesComidas)
                    vidaRecuperada = obj.vida
                    # Es el sushi!!!
                    if vidaRecuperada == 3:
                        self.score.setScore(SCORE_SUSHI)
                        if (self.v.getVida() >= 3):
                            self.grupoSpritesComidas.remove(obj)
                            self.grupoSprites.remove(obj)

                    if (self.v.getVida()< 3):
                        if (self.v.getVida()+vidaRecuperada > 3): #compruebo que no me paso de 3 a la hora de sumarle el total
                            #corregir esto...me suma lo que le peta
                            self.v.setVida(3)
                        else:
                            self.v.sumarVida(vidaRecuperada)

                        self.grupoSpritesComidas.remove(obj)
                        self.grupoSprites.remove(obj)


    #Proyectil enemigo sobre plataforma
                for pEnem in iter(self.grupoProyectilesEnemigo):
                    for i in [0,1,2]:
                        if pygame.sprite.spritecollideany (pEnem, self.grupoBloques[i]) != None:
                            self.grupoProyectilesEnemigo.remove(pEnem)
                            self.grupoSprites.remove(pEnem)

    #Proyectil amigo sobre plataforma

                for pAmigo in iter(self.grupoProyectilesAmigo):
                    for i in [0,1,2]:
                        if pygame.sprite.spritecollideany (pAmigo, self.grupoBloques[i]) != None:
                            self.grupoProyectilesAmigo.remove(pAmigo)
                            self.grupoSprites.remove(pAmigo)


    #Proyectil enemigo sobre jugador
                if pygame.sprite.spritecollideany (self.jugador1, self.grupoProyectilesEnemigo) != None:
                    obj = pygame.sprite.spritecollideany (self.jugador1, self.grupoProyectilesEnemigo)
                    self.grupoProyectilesEnemigo.remove(obj)
                    self.grupoSprites.remove(obj)
                    if (self.invulnerable > 99 ):

                        if (self.v.getVida()-1 < 0): #muerto
                            #self.director.salirEscena()
                            self.death = True
                        self.v.quitarVida(1)
                        self.invulnerable = 0


    #Proyectil jugador sobre enemigo vidaEnemigoMacarra

                for pAmigo in iter(self.grupoProyectilesAmigo):
                    if pygame.sprite.spritecollideany (pAmigo, self.grupoEnemigos) != None:
                        obj = pygame.sprite.spritecollideany (pAmigo, self.grupoEnemigos)
                        #vida1 = vidaEnemigoMacarra (obj)

                        if (obj.devolverId() != 1):
                            if (obj.getVida() - 1 <=0):
                                self.grupoProyectilesAmigo.remove(pAmigo)
                                self.grupoEnemigos.remove (obj)
                                self.grupoSprites.remove(obj)
                                self.grupoSprites.remove(pAmigo)
                                #Sumo puntuacion
                                self.score.setScore(SCORE_ENEMIGO)
                            else:
                                obj.danoRecibido(1)
                                self.grupoProyectilesAmigo.remove(pAmigo)
                                self.grupoSprites.remove(pAmigo)
                        else:
                            if (self.vB.getVida() == 0): # la vida del jefe es la 0 y le vuelves a dar
                                self.vB.quitarVida(1)
                                self.grupoProyectilesAmigo.remove(pAmigo)
                                self.grupoEnemigos.remove (obj)
                                self.grupoSprites.remove(obj)
                                self.grupoSprites.remove(pAmigo)
                                self.director.nivelActual +=1
                                self.director.cambiarEscena (Fase (self.director))
                            else:
                                self.vB.quitarVida(1)
                                self.grupoProyectilesAmigo.remove(pAmigo)
                                self.grupoSprites.remove(pAmigo)



    #pygame.sprite.groupcollide
                if self.jugador1 != None:
                    self.invulnerable += 1

                self.GameOver.update(self.death)
                self.actualizarScroll(self.jugador1)

            else:
                # boolean if conversacion ha acabado
		if (self.decorado.parser.nombreDecorado == 'conversa' and self.decorado.dialogo.masTextos == False ):
                	self.director.nivelActual +=1
                    	self.director.cambiarEscena (Fase (self.director))

    def dibujar(self, pantalla):

        # Ponemos primero el fondo
        #self.menuInferior.dibujar(pantalla)
        pantalla.fill((0,0,0))
        # Después el decorado
        self.decorado.dibujar(pantalla)
        # Luego los Sprites
        self.grupoSprites.draw(pantalla)
        self.pausa.draw(pantalla)
        self.GameOver.draw(pantalla)
        self.score.draw(pantalla)




        if self.boss != None:
            self.vB.update(pantalla)

        #  Funcionalidades para los niveles
        if self.jugador1 != None:
            self.v.update(pantalla)
            if self.jugador1.getGodMode():
                fuente = pygame.font.SysFont('arial', 28);
                imagen = fuente.render("GODMODE", True, (200,220,200))
                rect = imagen.get_rect()
                rect.left = ANCHO_PANTALLA - 180
                rect.top =  50
                pantalla.blit(imagen,rect)


        if (self.decorado.parser.nombreDecorado == 'guion'):
            self.decorado.dibujar(pantalla)
        if (self.decorado.parser.nombreDecorado == 'conversa'):
            if (self.decorado.dialogo.masTextos):
                self.decorado.dialogo.dibujar(pantalla)
                self.decorado.dialogo.update()
                pygame.time.wait(3000)

    def eventos(self, lista_eventos):
        # Miramos a ver si hay algun evento de salir del programa
        for evento in lista_eventos:
            # Si se quiere salir, se le indica al director
            if evento.type == pygame.QUIT:
                self.director.salirPrograma()

        # Indicamos la acción a realizar segun la tecla pulsada para cada jugador
        teclasPulsadas = pygame.key.get_pressed()

        # Esperamos que el jugador pulse algo para empezar la accion
        if self.iniciate == False:
            for tecla in teclasPulsadas:
                if tecla == True:
                    self.iniciate= True
                    break


        if teclasPulsadas[K_q]:
            print(pygame.mouse.get_pos());
        if teclasPulsadas[K_p]:
            self.director.nivelActual +=1
            self.director.cambiarEscena (Fase (self.director))




        #Pause...con tabulacion

        if teclasPulsadas[K_TAB]:
            self.pausa.updatePausa(teclasPulsadas, K_TAB)
        else:
            self.pausa.setPausaUltima(False)

        if (self.pausa.getPausa() == False) and (self.death == False) and (self.jugador1 != None):

            self.jugador1.mover(teclasPulsadas, K_UP, K_DOWN, K_LEFT, K_RIGHT)
            self.jugador1.disparar(teclasPulsadas, K_SPACE, self.grupoProyectilesAmigo)
            if teclasPulsadas[K_y]:
                print("wtff")
                self.v.quitarVida(1)
                self.invulnerable = 0
            if teclasPulsadas[K_g]:
                self.jugador1.setGodMode();
            else:
                self.jugador1.setLastGodMode();





# Clases que se utilizan para instanciar elementos dentro de fase, como score o
# otros elementos gráficos como decorado,  gameOver, pausa


# Singleton que se utiliza para que solo haya una puntuacion en todas las fases
class ScoreTon:

    instance = None

    @classmethod
    def getInstance(cls):
        if ScoreTon.instance == None:
            ScoreTon.instance = ScoreTon._ScoreTon()

            return ScoreTon.instance
        else:

            return ScoreTon.instance


    # Esta clase privada de instancia unica que guarda la informacion del score
    class _ScoreTon:
        def __init__(self):
            self.score = 0

        def __str__(self):
            return "Score : " + str(self.score)

        def iniciarScore(self):
            self.score = 0

        def setScore(self, type, time = 0):

            if type == SCORE_TIME:

                self.score +=  (1000000/ time)

            if type == SCORE_ENEMIGO:
                self.score += 100

            if type == SCORE_SUSHI:
                self.score += 500

            if type == SCORE_BOSS:
                self.score += 750

            if type == SCORE_DEATH:
                self.score -= 1000
                if self.score < 0:
                    self.iniciarScore()

        def draw(self, pantalla):
            fuente = pygame.font.SysFont('arial', 28);
            imagen = fuente.render("Score : " + str(self.score), True, (200,220,200))
            rect = imagen.get_rect()
            rect.left = ANCHO_PANTALLA - 180
            rect.top = 0
            pantalla.blit(imagen,rect)




#------------------------_
class GameOver:
    def __init__(self, director):
        self.imagen = GestorRecursos.CargarImagen("game_over.png", -1)
        self.pantallaGameOver = 0
        self.death = False
        self.director = director
    def update(self, death):

        self.death = death
        if death == True:
            self.pantallaGameOver += 1

        if (self.pantallaGameOver > 100):
            self.director.salirEscena()

    def draw(self, pantalla):
        if self.death == True:
            pantalla.blit (self.imagen, (ANCHO_PANTALLA/10,0))


#--------------------------------------------------
class Pausa:
    def __init__(self):
        self.pausa = False
        self.ultimaPulsacionPausa= False
        self.imagen = GestorRecursos.CargarImagen("pausa.png")
        self.tiempoPausa = 0

    def updatePausa(self, teclasPulsadas, tab):
        if self.ultimaPulsacionPausa==False:
            self.pausa = not self.pausa
            print(self.pausa)


        self.ultimaPulsacionPausa=True
        if self.pausa == True:
            self.tiempoPausa+=1

    def setPausaUltima(self, pausa):
        self.ultimaPulsacionPausa = pausa

    def getPausa(self):
        return self.pausa

    def draw(self, pantalla):
        if self.pausa == True:
            pantalla.blit (self.imagen, (ANCHO_PANTALLA-50,ALTO_PANTALLA-50))



# -------------------------------------------------
# Clase Decorado
class Decorado:
    def __init__(self, parser):

	self.parser=parser
        posDecorado = parser.posDecorado
	self.controlConversa =0
        self.x = posDecorado[0]
        self.y = posDecorado[1]
        self.imagen = GestorRecursos.CargarImagen(parser.decorado, -1)

        self.rect = self.imagen.get_rect()

	if (parser.nombreDecorado == 'conversa'):

		self.imagen = pygame.transform.scale(self.imagen,(ANCHO_PANTALLA,ALTO_PANTALLA))
		self.dialogo = Dialogos (parser)


        self.rect = pygame.Rect(0, self.y, ANCHO_PANTALLA, (self.imagen.get_rect().bottom-self.y))
        self.scrolly_f = 0





    def update(self, scrollx, scrolly):

          self.rect.top = scrolly + self.y
          self.rect.left = scrollx + self.x




    def dibujar(self,pantalla):

        pantalla.blit(self.imagen,(0,0),self.rect)


#VIDA BOSS
class vidaBoss:
    def __init__(self, boss):
        self.boss = boss

        self.death =  GestorRecursos.CargarImagen('life/boss0.jpg', -1)

        self.life1 = GestorRecursos.CargarImagen('life/boss.jpg', -1)
        self.life2 = GestorRecursos.CargarImagen('life/boss1.jpg', -1)
        self.life3 = GestorRecursos.CargarImagen('life/boss2.jpg', -1)
        self.life4 = GestorRecursos.CargarImagen('life/boss3.jpg', -1)
        self.life5 = GestorRecursos.CargarImagen('life/boss4.jpg', -1)
        self.life6 = GestorRecursos.CargarImagen('life/boss5.jpg', -1)
        self.life7 = GestorRecursos.CargarImagen('life/boss6.jpg', -1)
        self.life8 = GestorRecursos.CargarImagen('life/boss7.jpg', -1)
        self.life9 = GestorRecursos.CargarImagen('life/boss8.jpg', -1)
        self.life10 = GestorRecursos.CargarImagen('life/boss9.jpg', -1)
        self.life11 = GestorRecursos.CargarImagen('life/boss10.jpg', -1)


        self.inicial = self.life11
        self.rectang= pygame.Rect(300,0, 300, 100)
        self.life = self.boss.vida


    def getVida (self):
        return self.life

    def quitarVida (self, vida):

        self.life = self.boss.danoRecibido(vida)


    def update (self,pantalla):
        if (self.life == 11):
            self.inicial = self.life11
            self.dibujar(pantalla)
        if (self.life == 10):
            self.inicial = self.life10
            self.dibujar(pantalla)
        if (self.life == 9):
            self.inicial = self.life9
            self.dibujar(pantalla)
        if (self.life == 8):
            self.inicial = self.life8
            self.dibujar(pantalla)
        if (self.life == 7):
            self.inicial = self.life7
            self.dibujar(pantalla)
        if (self.life == 6):
            self.inicial = self.life6
            self.dibujar(pantalla)
        if (self.life == 5):
            self.inicial = self.life5
            self.dibujar(pantalla)
        if (self.life == 4):
            self.inicial = self.life4
            self.dibujar(pantalla)
        if (self.life == 3):
            self.inicial = self.life3
            self.dibujar(pantalla)
        if (self.life == 2):
            self.inicial = self.life2
            self.dibujar(pantalla)
        if (self.life == 1):
            self.inicial = self.life1
            self.dibujar(pantalla)
        if (self.life == 0):
            self.inicial = self.death
            self.dibujar(pantalla)
        if (self.life < 0):
            self.inicial = None


    def dibujar(self, pantalla):
        pantalla.blit(self.inicial, self.rectang)

###########################################

class vidaPersonaje:
    def __init__(self, jugadores):
        self.jugador = jugadores

        self.death = GestorRecursos.CargarImagen('life/death.png', -1)
        self.life1 = GestorRecursos.CargarImagen('life/life1.png', -1)
        self.life2 = GestorRecursos.CargarImagen('life/life2.png', -1)
        self.life3 = GestorRecursos.CargarImagen('life/life3.png', -1)
        self.inicial = self.life3
        self.rectang= pygame.Rect(0,0, 100, 50)
        self.life = self.jugador.vida

    def setVida (self, cantidad):
        self.life = cantidad
        self.jugador.vida = self.life
    def getVida (self):
        return self.life

    def sumarVida (self, vida):
        self.life = self.jugador.curarse(vida)


    def quitarVida (self, vida):
        self.life = self.jugador.danoRecibido(vida)


    def update (self,pantalla):
        if (self.life == 3):
            self.inicial = self.life3
        if (self.life == 2):
            self.inicial = self.life2
        if (self.life == 1):
            self.inicial = self.life1
        if (self.life == 0):
            self.inicial = self.death

        self.dibujar(pantalla)
    def dibujar(self, pantalla):
        pantalla.blit(self.inicial, self.rectang)

class Dialogos:
    def __init__(self, parser):
	self.parser = parser
	self.cuadro = pygame.Surface((ANCHO_PANTALLA,100))
	self.cuadroRect = self.cuadro.fill((0,0,0))
	self.cuadroRect.top = ALTO_PANTALLA-80
	self.masTextos = True
    	self.fuente = pygame.font.SysFont('arial', 18);
    	self.imagenX = self.fuente.render(parser.listaTextos[0], True, (255,255,255))
	self.textoActual=0
    	self.rect = self.imagenX.get_rect()
    	self.rect.left = 40
    	self.rect.top = ALTO_PANTALLA-80+40
	self.masTextos =True



    def update (self):
	if (self.textoActual +1 <=  self.parser.maxTextos):
		#print "22222222222222222 " + str (self.textoActual)
		self.imagenX = self.fuente.render(self.parser.listaTextos[self.textoActual], True, (255,255,255))

        	self.textoActual = self.textoActual +1
	else:
		self.masTextos = False




    def dibujar(self, pantalla):
	pantalla.blit(self.cuadro, self.cuadroRect)
        pantalla.blit(self.imagenX, self.rect)
