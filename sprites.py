# -*- coding: utf-8 -*-

import pygame, sys, os, math, time, random
from pygame.locals import *
from escena import *
from gestorRecursos import *
# -------------------------------------------------
# -------------------------------------------------
# Constantes
# -------------------------------------------------
# -------------------------------------------------

# Colores para debugear
NEGRO  = (  0,   0,   0)
BLANCO = (255, 255, 255)
VERDE  = (0,   255,   0)
ROJO   = (255,   0,   0)
AZUL   = (0,   0,   255)

# Movimientos y Colisiones
QUIETO = NOCOLISION = 0
IZQUIERDA = 1
DERECHA = 2
ARRIBA = 3
ABAJO = 4


#Posturas
SPRITE_QUIETO = 0
SPRITE_ANDANDO = 1
SPRITE_SALTANDO = 2
SPRITE_MUERTO = 3


SKINWIDTH = 1.2 # Pixeles de umbral

# Velocidades de los distintos personajes
VELOCIDAD_JUGADOR = 0.2 # Pixeles por milisegundo
VELOCIDAD_SALTO_JUGADOR = 0.3 # Pixeles por milisegundo
RETARDO_ANIMACION_JUGADOR = 3 # updates que durará cada imagen del personaje
                              # debería de ser un valor distinto para cada postura

VELOCIDAD_SNIPER = 0.12 # Pixeles por milisegundo
VELOCIDAD_SALTO_SNIPER = 0.27 # Pixeles por milisegundo
RETARDO_ANIMACION_SNIPER = 5 # updates que durará cada imagen del personaje
DISTANCIA_VISIBILIDAD = 0 # pixeles


VELOCIDAD_PROJECTIL = 1 # Pixel por segundo
GRAVEDAD = 0.0004 # Píxeles / ms2

# -------------------------------------------------
# -------------------------------------------------
# Clases de los sprites del juego
# -------------------------------------------------
# -------------------------------------------------


# -------------------------------------------------
# Clase MiSprite
class MiSprite(pygame.sprite.Sprite):
    "Los Sprites que tendra este juego"
    def __init__(self, grupoSprites):
        pygame.sprite.Sprite.__init__(self)
        self.posicion = (0, 0)
        self.velocidad = (0, 0)
        self.scroll   = (0, 0)
        grupoSprites.add(self)
        self.grupoSprites = grupoSprites


    def establecerPosicion(self, posicion):
        self.posicion = posicion
        self.rect.left = self.posicion[0] - self.scroll[0]
        self.rect.bottom = self.posicion[1] - self.scroll[1]


    def establecerPosicionPantalla(self, scrollDecorado):
        self.scroll = scrollDecorado;
        (scrollx, scrolly) = self.scroll;
        (posx, posy) = self.posicion;
        self.rect.left = posx - scrollx;
        self.rect.bottom = posy - scrolly;

    def incrementarPosicion(self, incremento):
        (posx, posy) = self.posicion
        (incrementox, incrementoy) = incremento
        self.establecerPosicion((posx+incrementox, posy+incrementoy))

    def update(self, tiempo):
        incrementox = self.velocidad[0]*tiempo
        incrementoy = self.velocidad[1]*tiempo
        self.incrementarPosicion((incrementox, incrementoy))








# -------------------------------------------------
# Clases Comidas

class Comidas(MiSprite):
    "Cualquier comida del juego"
    def __init__(self, grupoSprites, grupoSpritesComidas, posicion, archivoImagen):
        # Primero invocamos al constructor de la clase padre
        MiSprite.__init__(self, grupoSprites);
        grupoSpritesComidas.add(self)

        # Rectangulo con las coordenadas en pantalla que ocupara
        self.rect = Rect(posicion, (10,10))
        # Y lo situamos de forma global en esas coordenadas
        self.establecerPosicion((self.rect.left, self.rect.bottom))
        self.image = GestorRecursos.CargarImagen (archivoImagen, -1)


class Cake(Comidas):
    "mmmm...rico pastel"

    def __init__(self, grupoSprites, grupoSpritesComidas, posicion):
        Comidas.__init__(self,grupoSprites, grupoSpritesComidas, posicion, 'food/cake.png');
        self.vida = 2


class Hamburger(Comidas):
    "sabrosa hamburguesa"
    def __init__(self, grupoSprites, grupoSpritesComidas, posicion):
        Comidas.__init__(self,grupoSprites, grupoSpritesComidas, posicion, 'food/burguer.png');
        self.vida = 1



class Susi(Comidas):

    def __init__(self, grupoSprites, grupoSpritesComidas, posicion):
        Comidas.__init__(self,grupoSprites, grupoSpritesComidas, posicion, 'food/sushi.png');

        self.vida = 3




class Tea(Comidas):
    "pis de gato"
    def __init__(self, grupoSprites, grupoSpritesComidas, posicion):
        Comidas.__init__(self,grupoSprites, grupoSpritesComidas, posicion, 'food/tea.png');
        self.vida = 1





# -------------------------------------------------
# Clases Personaje

#class Personaje(pygame.sprite.Sprite):
class Personaje(MiSprite):
    "Cualquier personaje del juego"
    # Parametros pasados al constructor de esta clase:
    #  Archivo con la hoja de Sprites
    #  Archivo con las coordenadoas dentro de la hoja
    #  Numero de imagenes en cada postura
    #  Velocidad de caminar y de salto
    #  Retardo para mostrar la animacion del personaje

    def __init__(self, grupoSprites, grupoSpritesDinamicos, archivoImagen, archivoCoordenadas, numImagenes, velocidadCarrera, velocidadSalto, retardoAnimacion):
        # Primero invocamos al constructor de la clase padre
        MiSprite.__init__(self, grupoSprites);
        grupoSpritesDinamicos.add(self)

        # Guardamos referencias a los grupos para luego añadir las balas a ellos
        self.grupoSpritesDinamicos = grupoSpritesDinamicos

        # Se carga la hoja
        self.hoja = GestorRecursos.CargarImagen(archivoImagen,-1)
        self.hoja = self.hoja.convert_alpha()
        # El movimiento que esta realizando
        self.movimiento = QUIETO
        # Lado hacia el que esta mirando
        self.mirando = IZQUIERDA

        # Ultima Colision en el anterior frame


        # Leemos las coordenadas de un archivo de texto
        datos = GestorRecursos.CargarArchivoCoordenadas(archivoCoordenadas)
        datos = datos.split()
        self.numPostura = 1;
        self.numImagenPostura = 0;
        cont = 0;
        self.coordenadasHoja = [];
        for linea in range(0, 5):
            self.coordenadasHoja.append([])
            tmp = self.coordenadasHoja[linea]
            for postura in range(1, numImagenes[linea]+1):
                tmp.append(pygame.Rect((int(datos[cont]), int(datos[cont+1])), (int(datos[cont+2]), int(datos[cont+3]))))
                cont += 4
            #print("linea :  "  str(linea))

        # El retardo a la hora de cambiar la imagen del Sprite (para que no se mueva demasiado rápido)
        self.retardoMovimiento = 0;

        # En que postura esta inicialmente
        self.numPostura = QUIETO

        # El rectangulo del Sprite
        self.rect = pygame.Rect(100,100,self.coordenadasHoja[self.numPostura][self.numImagenPostura][2],self.coordenadasHoja[self.numPostura][self.numImagenPostura][3])

        # Las velocidades de caminar y salto
        self.velocidadCarrera = velocidadCarrera
        self.velocidadSalto = velocidadSalto

        # El retardo en la animacion del personaje (podria y deberia ser distinto para cada postura)
        self.retardoAnimacion = retardoAnimacion

        # Sonidos del juego
        self.jump_sound = GestorRecursos.load_sound("jump.ogg", 0.3)

        # Y actualizamos la postura del Sprite inicial, llamando al metodo correspondiente
        self.actualizarPostura()


    # Metodo base para realizar el movimiento: simplemente se le indica cual va a hacer, y lo almacena
    def mover(self, movimiento):

        if self.numPostura != SPRITE_MUERTO:
            if movimiento == ARRIBA:
                # Si estamos en el aire y el personaje quiere saltar, ignoramos este movimiento
                if self.numPostura == SPRITE_SALTANDO:
                    self.movimiento = QUIETO
                else:
                    self.movimiento = ARRIBA
                    self.jump_sound.play()
            else:
                self.movimiento = movimiento

    def setGrupo(self, grupo):
        self.grupoSprites = grupo

    def disparar(self,  grupoProyectiles, spriteBullet = 'bullet.png'):


        posicion = ((self.posicion[0]-self.scroll[0]),(self.posicion[1]-self.scroll[1]))
        speed = VELOCIDAD_PROJECTIL

        if (self.mirando != DERECHA):
            speed *= -1


        Projectile(posicion, speed, self.grupoSprites, self.grupoSpritesDinamicos, grupoProyectiles, spriteBullet)

    def actualizarPostura(self):
        self.retardoMovimiento -= 1
        # Miramos si ha pasado el retardo para dibujar una nueva postura

        if (self.retardoMovimiento < 0):
            self.retardoMovimiento = self.retardoAnimacion
            # Si ha pasado, actualizamos la postura
            self.numImagenPostura += 1


            if self.numImagenPostura >= len(self.coordenadasHoja[self.numPostura]):
                self.numImagenPostura = 0;
            if self.numImagenPostura < 0:
                self.numImagenPostura = len(self.coordenadasHoja[self.numPostura])-1

            self.image = self.hoja.subsurface(self.coordenadasHoja[self.numPostura][self.numImagenPostura])

            # Si esta mirando a la izquiera, cogemos la porcion de la hoja
            if self.mirando == IZQUIERDA:
                self.image = self.hoja.subsurface(self.coordenadasHoja[self.numPostura][self.numImagenPostura])
            #  Si no, si mira a la derecha, invertimos esa imagen
            elif self.mirando == DERECHA:
                self.image = pygame.transform.flip(self.hoja.subsurface(self.coordenadasHoja[self.numPostura][self.numImagenPostura]), 1, 0)


    def update(self, grupoBloques, tiempo):

          # Las velocidades a las que iba hasta este momento
          (velocidadx, velocidady) = self.velocidad

          # Guardo en listas las colisiones
          lista_impactos_muros = pygame.sprite.spritecollide(self, grupoBloques[1], False)
          numColisiones = len(lista_impactos_muros)

          lista_impactos_plataformas = pygame.sprite.spritecollide(self, grupoBloques[0], False)
          numColisionesP = len(lista_impactos_plataformas)


          if self.numPostura == SPRITE_MUERTO:
              self.actualizarPostura()
              self.velocidad = (0, -0.1)
              MiSprite.update(self, tiempo)
              return


          # Si vamos a la izquierda o a la derecha
          if (self.movimiento == IZQUIERDA) or (self.movimiento == DERECHA):
              # Esta mirando hacia ese lado
              self.mirando = self.movimiento

              # Si vamos a la izquierda, le ponemos velocidad en esa dirección
              if self.movimiento == IZQUIERDA:
                  velocidadx = -self.velocidadCarrera
              # Si vamos a la derecha, le ponemos velocidad en esa dirección
              else:
                  velocidadx = self.velocidadCarrera

              # Si no estamos en el aire
              if self.numPostura != SPRITE_SALTANDO:
              # La postura actual sera estar caminando
                  self.numPostura = SPRITE_ANDANDO

                  #Esto lo pone en el aire si se cumplen esta condicion:
                  # No esta encima de ninguna plataforma
                  if (numColisionesP == 0):
                      self.numPostura = SPRITE_SALTANDO
                  elif (numColisionesP < 2):
                      for plataforma in lista_impactos_plataformas:
                          if (plataforma.rect.top <= self.rect.bottom - SKINWIDTH):
                              self.numPostura = SPRITE_SALTANDO



              if numColisiones > 1:
                  #for impacto in lista_impactos_muros:
                      #print(impacto.nombre)
                  velocidadx = 0
              else:
                  for impacto in lista_impactos_muros:
                      #print(impacto.nombre)

                      if (velocidadx > 0) and (self.posicion[0] < impacto.posicion[0]):
                          velocidadx = 0

                      if (velocidadx < 0) and (self.posicion[0] > impacto.posicion[0]):
                          velocidadx = 0



          # Si queremos saltar
          elif self.movimiento == ARRIBA:
              # La postura actual sera estar saltando
              self.numPostura = SPRITE_SALTANDO
              # Le imprimimos una velocidad en el eje y
              velocidady = -self.velocidadSalto

          # Si no se ha pulsado ninguna tecla
          elif self.movimiento == QUIETO:
              # Si no estamos saltando, la postura actual será estar quieto
              if not self.numPostura == SPRITE_SALTANDO:
                  self.numPostura = SPRITE_QUIETO
              velocidadx = 0



          # Además, si estamos en el aire
          if self.numPostura == SPRITE_SALTANDO:

              # Miramos a ver si hay que parar de caer: si hemos llegado a una plataforma
              plataforma = pygame.sprite.spritecollideany(self, grupoBloques[0])

              #Tambien miramos si colisionamos algun muro, estos impiden el movimiento hacia arriba
              techo = pygame.sprite.spritecollideany(self, grupoBloques[2])

              # He chocado con un muro
              if (velocidady < 0) and (techo != None):
                  velocidady = 0



              #  las colisiones con plataformas solo nos interesa cuando estamos cayendo
              #  y solo es efectiva cuando caemos encima, no de lado, es decir,
              #  cuando nuestra posicion inferior esta por encima de la parte de abajo de la plataforma

              if (plataforma != None) and (velocidady>0) and (plataforma.rect.bottom>self.rect.bottom):
                  # Lo situamos con la parte de abajo un pixel colisionando con la plataforma
                  #  para poder detectar cuando se cae de ella
                  self.establecerPosicion((self.posicion[0], plataforma.posicion[1]-plataforma.rect.height+1))
                  # Lo ponemos como quieto
                  self.numPostura = SPRITE_QUIETO
                  # Y estará quieto en el eje y
                  velocidady = 0

              # Si no caemos en una plataforma, aplicamos el efecto de la gravedad
              else:
                  velocidady += GRAVEDAD * tiempo

          # Actualizamos la imagen a mostrar
          self.actualizarPostura()

          # Aplicamos la velocidad en cada eje
          self.velocidad = (velocidadx, velocidady)

          # Y llamamos al método de la superclase para que, según la velocidad y el tiempo
          #  calcule la nueva posición del Sprite
          MiSprite.update(self, tiempo)

          return


# -------------------------------------------------
# Clase Jugador

class Jugador(Personaje):
    "El jugador del juego"
    def __init__(self, grupoSprites, grupoSpritesDinamicos, grupoJugadores, grupoProyectilesAmigo):
        # Invocamos al constructor de la clase padre con la configuracion de este personaje concreto
        Personaje.__init__(self, grupoSprites, grupoSpritesDinamicos, 'neil2.png','coordNeil2.txt', [1, 4, 1, 1, 1], VELOCIDAD_JUGADOR, VELOCIDAD_SALTO_JUGADOR, RETARDO_ANIMACION_JUGADOR);
        self.vida = 3
        grupoJugadores.add(self)
        self.godMode = False
        self.lastGodMode = False

    def getVida (self):
        return self.vida

    def setGodMode(self):
        if self.lastGodMode==False:
            self.godMode = not self.godMode
        self.lastGodMode = True

    def getGodMode(self):
        return self.godMode


    def setLastGodMode(self):
        self.lastGodMode = False


    def danoRecibido (self, cantidad):
        if self.godMode != True:
            self.vida -= cantidad

        if self.vida < 0:
            self.numPostura = SPRITE_MUERTO


        return self.vida

    def curarse (self, cantidad):
        self.vida += cantidad
        return self.vida

    def mover(self, teclasPulsadas, arriba, abajo, izquierda, derecha):
        # Indicamos la acción a realizar segun la tecla pulsada para el jugador
        if teclasPulsadas[arriba]:
            Personaje.mover(self,ARRIBA)
        elif teclasPulsadas[izquierda]:
            Personaje.mover(self,IZQUIERDA)
        elif teclasPulsadas[derecha]:
            Personaje.mover(self,DERECHA)
        else:
            Personaje.mover(self,QUIETO)

    def disparar(self,teclasPulsadas, disparo, grupo):
        if (teclasPulsadas[disparo] and self.ultimaPulsacionDisparo == False):
            Personaje.disparar(self, grupo)

        if (teclasPulsadas[disparo]):
            self.ultimaPulsacionDisparo = True

        else:
            self.ultimaPulsacionDisparo = False


# -------------------------------------------------
# Clase NoJugador

class NoJugador(Personaje):
    "El resto de personajes no jugadores"
    def __init__(self, grupoSprites, grupoSpritesDinamicos, archivoImagen, archivoCoordenadas, numImagenes, velocidad, velocidadSalto, retardoAnimacion):
        # Primero invocamos al constructor de la clase padre con los parametros pasados
        Personaje.__init__(self, grupoSprites, grupoSpritesDinamicos, archivoImagen, archivoCoordenadas, numImagenes, velocidad, velocidadSalto, retardoAnimacion);

    # Aqui vendria la implementacion de la IA segun las posiciones de los jugadores
    # La implementacion por defecto, este metodo deberia de ser implementado en las clases inferiores
    #  mostrando la personalidad de cada enemigo
    def mover_cpu(self, jugador):
        # Por defecto un enemigo no hace nada
        #  (se podria programar, por ejemplo, que disparase al jugador por defecto)
        return

    def accion_cpu(self,jugador):
        # Por defecto no implementada, los enemigos su accion es atacarte y los neutrables inicialmente
        # conversacion
        return

# -------------------------------------------------
#clase Neutral
class Neutral(NoJugador):
    "El enemigo 'Neutral'"
    def __init__(self, grupoSprites, grupoSpritesDinamicos, grupoNeutrales):
        # Invocamos al constructor de la clase padre con la configuracion de este personaje concreto
        NoJugador.__init__(self, grupoSprites, grupoSpritesDinamicos, 'Sniper.png','coordSniper.txt', [5, 10, 6], 0, 0, RETARDO_ANIMACION_SNIPER);
        grupoNeutrales.add(self)

    # Aqui vendria la implementacion de la IA segun las posiciones de los jugadores
    # La implementacion de la inteligencia segun este personaje particular
    def mover_cpu(self, jugador1):
            Personaje.mover(self,QUIETO)


# Distancia

def calcDistance (object1, object2):

    return math.sqrt((abs(object1[0] - object2[0]) ** 2) + (abs(object1[1] - object2[1]) ** 2))


# -------------------------------------------------
# Clase Boss

class Boss(NoJugador):
    "El enemigo 'jefe' "
    def __init__(self, grupoSprites, grupoSpritesDinamicos, grupoEnemigos, grupoProyectilesEnemigo, identificador):
        # Invocamos al constructor de la clase padre con la configuracion de este personaje concreto
        NoJugador.__init__(self, grupoSprites, grupoSpritesDinamicos, 'MichaelJackson.png','coordMichael.txt', [1, 6, 1, 1, 0], VELOCIDAD_SNIPER, VELOCIDAD_SALTO_SNIPER, RETARDO_ANIMACION_SNIPER);
        grupoEnemigos.add(self)
        self.lastUpdate = 0
        self.vida = 11
        self.danhoRealizado = 2
        self.grupoProyectilesEnemigos = grupoProyectilesEnemigo
        self.identificador = identificador


    def danoRealizado (self):
        print ("hola")
        return self.danhoRealizado

    def getVida (self):
        return self.vida


    def devolverId (self):
        return self.identificador

    def danoRecibido (self, cantidad):
        self.vida -= cantidad

        if (self.vida <= 0):
            print("MORIIII!! "  + str(self.vida))
        return self.vida


    def mover_cpu(self, jugador):

    # Movemos solo a los enemigos que esten en la pantalla
    #if ((self.rect.left>0 and self.rect.right<ANCHO_PANTALLA and self.rect.bottom>0 and self.rect.top<ALTO_PANTALLA) and calcDistance(jugador.posicion, self.posicion) < 350 and jugador.posicion[1] == self.posicion[1]):

    # Y nos movemos andando hacia el
        salto = random.randrange(100) #Pares
        if salto == 5:
            Personaje.mover(self,ARRIBA)
        else:
            if jugador.posicion[0]<self.posicion[0] and (jugador.posicion[0] - self.posicion[0] - DISTANCIA_VISIBILIDAD):
                Personaje.mover(self,IZQUIERDA)
            else:
                Personaje.mover(self,DERECHA)

            newTime = float(time.clock())
            if ((newTime - self.lastUpdate) >= Macarra.interval):

                self.lastUpdate = newTime

                Personaje.disparar(self, self.grupoProyectilesEnemigos, 'sombrero.png')

    #else:
        #Personaje.mover(self,QUIETO)

# -------------------------------------------------
# Clase Macarra

class Macarra(NoJugador):

    interval = float(0.2)
    #"El enemigo 'Sniper'"
    def __init__(self, grupoSprites, grupoSpritesDinamicos, grupoEnemigos, grupoProyectilesEnemigo):
        # Invocamos al constructor de la clase padre con la configuracion de este personaje concreto
        NoJugador.__init__(self, grupoSprites, grupoSpritesDinamicos, 'gatoMacarra.png','coordMacarra.txt', [5, 6, 5, 4, 0], VELOCIDAD_SNIPER, VELOCIDAD_SALTO_SNIPER, RETARDO_ANIMACION_SNIPER);
        grupoEnemigos.add(self)
        self.grupoProyectilesEnemigos = grupoProyectilesEnemigo
        self.lastUpdate = 0
        self.vida = 2
        self.danhoRealizado = 1

    # Aqui vendria la implementacion de la IA segun las posiciones de los jugadores
    # La implementacion de la inteligencia segun este personaje particular
    def devolverId (self):
        return 2

    def getVida(self):
        return self.vida

    def danoRecibido (self, cantidad):
        self.vida -= cantidad

        if (self.vida <= 0):
            print("MORIIII!! "  + str(self.vida))
        return self.vida

    def danoRealizado (self):
        return self.danhoRealizado

    def mover_cpu(self, jugador):

        # Movemos solo a los enemigos que esten en la pantalla
        if ((self.rect.left>0 and self.rect.right<ANCHO_PANTALLA and self.rect.bottom>0 and self.rect.top<ALTO_PANTALLA) and calcDistance(jugador.posicion, self.posicion) < 350 and jugador.posicion[1] == self.posicion[1]  and jugador.posicion[1] - 5 <= self.posicion[1] and jugador.posicion[1] + 5 >= self.posicion[1]):

            # Y nos movemos andando hacia el
            if jugador.posicion[0]<self.posicion[0] and (jugador.posicion[0] - self.posicion[0] - DISTANCIA_VISIBILIDAD):
                Personaje.mover(self,IZQUIERDA)
            else:
                Personaje.mover(self,DERECHA)

            newTime = float(time.clock())
            if ((newTime - self.lastUpdate) >= Macarra.interval):

                self.lastUpdate = newTime

                Personaje.disparar(self, self.grupoProyectilesEnemigos)


        # Si este personaje no esta en pantalla, no hara nada
        else:
            Personaje.mover(self,QUIETO)


#--------------------------------------------------
# Clase Torreta

class Torreta(NoJugador):

    interval = float(0.2)
    #"El enemigo 'Sniper'"
    def __init__(self, grupoSprites, grupoSpritesDinamicos, grupoEnemigos, grupoProyectilesEnemigo):
        # Invocamos al constructor de la clase padre con la configuracion de este personaje concreto
        NoJugador.__init__(self, grupoSprites, grupoSpritesDinamicos, 'Sniper.png','coordSniper.txt', [5, 10, 6, 1, 0, 0, 0], VELOCIDAD_SNIPER, VELOCIDAD_SALTO_SNIPER, RETARDO_ANIMACION_SNIPER);
        grupoEnemigos.add(self)
        self.grupoProyectilesEnemigos = grupoProyectilesEnemigo
        self.lastUpdate = 0
        self.vida = 1
        self.danhoRealizado = 1
    # Aqui vendria la implementacion de la IA segun las posiciones de los jugadores
    # La implementacion de la inteligencia segun este personaje particular
    def disparar(self, jugador, grupo):
         if jugador.posicion[0]<self.posicion[0] and (jugador.posicion[0] - self.posicion[0] - DISTANCIA_VISIBILIDAD) :
             Personaje.disparar(self, grupo)

    def getVida(self):
        return self.vida

    def danoRealizado (self):
        return self.danhoRealizado

    def devolverId (self):
        return 2

    def danoRecibido (self, cantidad):
        self.vida -= cantidad

        if (self.vida <= 0):
            print("MORIIII!! "  + str(self.vida))
        return self.vida

    def mover_cpu(self, jugador):

        # Movemos solo a los enemigos que esten en la pantalla
        if ((self.rect.left>0 and self.rect.right<ANCHO_PANTALLA and self.rect.bottom>0 and self.rect.top<ALTO_PANTALLA) and calcDistance(jugador.posicion, self.posicion) < 350 and jugador.posicion[1] - 5 <= self.posicion[1] and jugador.posicion[1] + 5 >= self.posicion[1]):

            newTime = float(time.clock())
            if ((newTime - self.lastUpdate) >= Torreta.interval):

                self.lastUpdate = newTime
                self.disparar(jugador, self.grupoProyectilesEnemigos)

        # Si este personaje no esta en pantalla, no hara nada
        else:
            Personaje.mover(self,QUIETO)



# -------------------------------------------------
# Clase Bloque

class MiBloque(MiSprite):
    def __init__(self, grupoSprites, rectangulo, nombre, draw = 0):
        # Primero invocamos al constructor de la clase padre
        MiSprite.__init__(self, grupoSprites);

        # Rectangulo con las coordenadas en pantalla que ocupara
        self.rect = rectangulo
        self.nombre = nombre
        # Y lo situamos de forma global en esas coordenadas
        self.establecerPosicion((self.rect.left, self.rect.bottom))
        # En el caso particular de este juego, las plataformas no se van a ver, asi que no se carga ninguna imagen
        self.image = pygame.Surface((0, 0))

        if draw == 1:
            self.dibujar()

    def dibujar(self, color = VERDE):
        self.image = pygame.Surface([self.rect[2], self.rect[3]])
        self.image.fill(color)



# -------------------------------------------------
# Clase Plataforma

class Plataforma(MiBloque):
    def __init__(self, grupoSprites, grupoPlataformas, rectangulo, nombre, draw):
        # Primero invocamos al constructor de la clase padre
        MiBloque.__init__(self, grupoSprites, rectangulo, nombre, draw);
        grupoPlataformas.add(self)

#--------------------------------------------------

class Muro(MiBloque):
    def __init__(self, grupoSprites, grupoMuros, rectangulo, nombre, draw = 1):
        # Primero invocamos al constructor de la clase padre
        MiBloque.__init__(self, grupoSprites, rectangulo, nombre, draw);
        grupoMuros.add(self)

        if draw == 1:
            MiBloque.dibujar(self, BLANCO)

#--------------------------------------------------

class Techo(MiBloque):
    def __init__(self, grupoSprites, grupoTechos, rectangulo, nombre, draw = 1):
        # Primero invocamos al constructor de la clase padre
        MiBloque.__init__(self, grupoSprites, rectangulo, nombre, draw);
        grupoTechos.add(self)

        if draw == 1:
            MiBloque.dibujar(self, AZUL)

# -------------------------------------------------
# Clase FinFase

class FinFase(MiBloque):

    def __init__(self, grupoSprites, grupoFinFase, rectangulo, draw = 0):
        MiBloque.__init__(self, grupoSprites,rectangulo, 'Fin de Fase');
        grupoFinFase.add(self)

        if draw == 1:
            MiBloque.dibujar(self, ROJO)
# --------------------------------------------
# Clase Proyectiles

class Projectile(MiSprite):

    def __init__ (self, posicion, speed, grupoSprites, grupoSpritesDinamicos, grupo, spriteBullet):

        MiSprite.__init__(self, grupoSprites);

        x = posicion[0]
        y = posicion[1]

        self.velocidad = (speed, 0)

        # Rectangulo con las coordenadas en pantalla que ocupara
        self.rect = Rect((x,y-20), (13,13))
        # Y lo situamos de forma global en esas coordenadas
        self.establecerPosicion((self.rect.left, self.rect.bottom))
        self.image = GestorRecursos.CargarImagen (spriteBullet, -1)
        grupoSpritesDinamicos.add(self)
        grupo.add(self)

        self.grupoSprites = grupoSprites
        self.grupoSpritesDinamicos = grupoSpritesDinamicos
        self.grupo = grupo

    def update(self, grupoBloques, tiempo):
        MiSprite.update(self, tiempo)
        #Proyectil jugador sobre plataforma
        if pygame.sprite.spritecollideany (self, grupoBloques[0]) != None:
            self.grupoSprites.remove (self)
        if pygame.sprite.spritecollideany (self, grupoBloques[1]) != None:
            self.grupoSprites.remove (self)

        if (self.rect.left < 0) or (self.rect.right > ANCHO_PANTALLA):
            self.grupoSpritesDinamicos.remove(self)
            self.grupo.remove(self)
            self.grupoSprites.remove(self)

class FactoriaSprite(object):
    """Esta clase es nuestra factoria, para crear diferentes sprites segun parametros"""

    @classmethod
    def getPersonaje(self, nombre, grupoSprites, grupoSpritesDinamicos, grupoNeutrales, grupoEnemigos, grupoJugadores, grupoProyectilesAmigo,grupoProyectilesEnemigo ):
        "Metodo que instancia personajes segun el nombre"

        if (nombre == 'Megan'):
            return Neutral(grupoSprites, grupoSpritesDinamicos, grupoNeutrales)
        elif (nombre == 'Macarra'):
            return Macarra(grupoSprites, grupoSpritesDinamicos, grupoEnemigos, grupoProyectilesEnemigo)
        elif (nombre == 'Jugador'):
            return Jugador(grupoSprites, grupoSpritesDinamicos, grupoJugadores, grupoProyectilesAmigo)
        elif (nombre == 'Torreta'):
            return Torreta (grupoSprites, grupoSpritesDinamicos, grupoEnemigos, grupoProyectilesEnemigo)
        elif (nombre == 'Boss'):
            print("aaaaccc")
            return Boss (grupoSprites, grupoSpritesDinamicos, grupoEnemigos, grupoProyectilesEnemigo, 1)
    @classmethod
    def getBloque(self, grupoSprites, grupoBloques, bloque, draw = 0):
        """Metodo que instancia bloques segun el nombre
            grupos que se le pasan para añadirlos
            bloque es una plataforma o muro
            draw = 1, la opcion de dibujar el rect
        """
        rect = bloque.getRect()
        nombre = bloque.getNombre()
        tipo = bloque.getTipo()

        if (tipo == 'muro'):
            return Muro(grupoSprites, grupoBloques[1], rect, nombre, draw)

        if (tipo == 'techo'):
            return Techo(grupoSprites, grupoBloques[2], rect, nombre, draw)

        else: return Plataforma(grupoSprites, grupoBloques[0], rect, nombre, draw)

    @classmethod
    def getFood(self,tipo, grupoSprites, grupoFood, posicion):
        if (tipo == 'cake'):
            return Cake (grupoSprites, grupoFood, posicion)
        elif (tipo == 'tea'):
            return Tea (grupoSprites, grupoFood, posicion)
        elif (tipo == 'hamburgersa'):
            return Hamburger (grupoSprites, grupoFood, posicion)
        elif (tipo == 'sushi'):
            return Susi (grupoSprites, grupoFood, posicion)
