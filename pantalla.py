# -*- encoding: utf-8 -*-

import pygame
from pygame.locals import *
from gestorRecursos import *
from elementogui import *
from escena import *
class Pantalla:
    def __init__(self, menu, nombreImagen):
        self.menu = menu
        # Se carga la imagen de fondo
        self.imagen = GestorRecursos.CargarImagen(nombreImagen)
        self.imagen = pygame.transform.scale(self.imagen, (ANCHO_PANTALLA, ALTO_PANTALLA))
        # Se tiene una lista de elementos GUI
        self.elementosGUI = []

    def eventos(self, lista_eventos):
        for evento in lista_eventos:
            if evento.type == MOUSEBUTTONDOWN:
                self.elementoClic = None
                for elemento in self.elementosGUI:
                    if elemento.posicionEnElemento(evento.pos):
                        self.elementoClic = elemento
            elif evento.type == MOUSEBUTTONUP:
                for elemento in self.elementosGUI:
                    if elemento.posicionEnElemento(evento.pos):
                        if (elemento == self.elementoClic):
                            elemento.accion()
            elif evento.type == KEYDOWN:
           		if evento.key == K_UP:
					for elemento in self.elementosGUI:
						if(elemento.nombre=="images2.png"):
							elemento.accion("up")
		        if evento.key == K_RETURN:
		        	for elemento in self.elementosGUI:
		        		if(elemento.nombre=="images2.png"):
		        			elemento.accion("enter")
		        if evento.key == K_DOWN:
					for elemento in self.elementosGUI:
						if(elemento.nombre=="images2.png"):
							elemento.accion("down")
    def dibujar(self, pantalla):
        # Dibujamos primero la imagen de fondo
        pantalla.blit(self.imagen, self.imagen.get_rect())
        # Después los botones
        for elemento in self.elementosGUI:
        	elemento.dibujar(pantalla)
