# -*- encoding: utf-8 -*-

import pygame
from pygame.locals import *
from gestorRecursos import *
from pantalla import *


# -------------------------------------------------
# Clase abstracta ElementoGUI

class ElementoGUI:
    def __init__(self, pantalla, rectangulo, nombre):
        self.pantalla = pantalla
        self.rect = rectangulo
        self.nombre = nombre

    def establecerPosicion(self, posicion):
        (posicionx, posiciony) = posicion
        self.rect.left = posicionx
        self.rect.bottom = posiciony

    def posicionEnElemento(self, posicion):
        (posicionx, posiciony) = posicion
        if (posicionx>=self.rect.left) and (posicionx<=self.rect.right) and (posiciony>=self.rect.top) and (posiciony<=self.rect.bottom):
            return True
        else:
            return False

    def buscarPosicion(self):
    	posicionx = self.rect.left
    	posiciony = self.rect.bottom
    	posicion = (posicionx,posiciony)
    	return posicion

	"""def establecerNombre(self,nombre):
		self.nombre = nombre """




# -------------------------------------------------
# Clase Boton y los distintos botones

class Boton(ElementoGUI):
    def __init__(self, pantalla, nombreImagen, posicion):
        # Se carga la imagen del boton
        self.imagen = GestorRecursos.CargarImagen(nombreImagen,-1)
        self.imagen = pygame.transform.scale(self.imagen, (50, 50))
        # Se llama al método de la clase padre con el rectángulo que ocupa el botón
        ElementoGUI.__init__(self, pantalla, self.imagen.get_rect(), nombreImagen)
        # Se coloca el rectangulo en su posicion
        self.establecerPosicion(posicion)
    def dibujar(self, pantalla):
        pantalla.blit(self.imagen, self.rect)

class BotonMover(Boton):
    def __init__(self, pantalla,nombreImagen,posicion):
        Boton.__init__(self, pantalla, nombreImagen, posicion)
    def accion(self,tecla):
    	(posicionx,posiciony) = self.buscarPosicion()
    	if(tecla=="down"):
            if(posiciony<270):
    			self.establecerPosicion((posicionx,posiciony+60))
            else:
    			self.establecerPosicion((posicionx,215))
    	if(tecla=="up"):
    		if(posiciony>215):
    			self.establecerPosicion((posicionx,posiciony-60))
    		else:
    			self.establecerPosicion((posicionx,275))
    	if(tecla=="enter"):
   			print(posiciony)
   			if(posiciony<270):
   				self.pantalla.menu.ejecutarJuego()
   			else:
   				self.pantalla.menu.salirPrograma()

class BotonSalir(Boton):
    def __init__(self, pantalla,nombreImagen,posicion):
        Boton.__init__(self, pantalla, nombreImagen, posicion)
    def accion(self):
        self.pantalla.menu.salirPrograma()

# -------------------------------------------------
# Clase TextoGUI y los distintos textos

class TextoGUI(ElementoGUI):
    def __init__(self, pantalla, fuente, color, texto, posicion):
        # Se crea la imagen del texto
        self.imagen = fuente.render(texto, True, color)
        # Se llama al método de la clase padre con el rectángulo que ocupa el texto
        ElementoGUI.__init__(self, pantalla, self.imagen.get_rect(),texto)
        # Se coloca el rectangulo en su posicion
        self.establecerPosicion(posicion)
    def dibujar(self, pantalla):
        pantalla.blit(self.imagen, self.rect)

class TextoJugar(TextoGUI):
    def __init__(self, pantalla,texto,posicion):
        # La fuente la debería cargar el estor de recursos
        fuente = pygame.font.SysFont('timesNewRoman', 46);
        TextoGUI.__init__(self, pantalla, fuente, (255, 255, 200), texto, posicion)
    def accion(self):
        self.pantalla.menu.ejecutarJuego()

class TextoSalir(TextoGUI):
    def __init__(self, pantalla,texto,posicion):
        # La fuente la debería cargar el estor de recursos
        fuente = pygame.font.SysFont('timesNewRoman', 46);
        TextoGUI.__init__(self, pantalla, fuente, (255, 255, 200), texto, posicion)
    def accion(self):
        self.pantalla.menu.salirPrograma()


class Icono (ElementoGUI):
	def __init__(self,pantalla, nombreImagen, posicion):
		self.imagen = GestorRecursos.CargarImagen(nombreImagen, -1)
		self.imagen = pygame.transform.scale(self.imagen, (100,100))
		ElementoGUI.__init__(self, pantalla, self.imagen.get_rect(),nombreImagen)
		self.establecerPosicion(posicion)
	def dibujar(self, pantalla):
		pantalla.blit(self.imagen, self.rect)

class IconoPrincipal(Icono):
	def __init__(self, pantalla, nombreImagen, posicion):
		Icono.__init__(self, pantalla, nombreImagen, posicion)

# -------------------------------------------------
# Clase IconoGUI y los distintos iconos
