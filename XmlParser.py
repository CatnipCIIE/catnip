#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Importar modulos


from etree import ElementTree as etree



from sprites import *
import pygame


class XmlParser:

	def __init__(self):
		self.leerConfiguracion("properties.xml")


	def devolverNivel (self,numero_nivel):


		doc = etree.parse(self.faseActual)
		raiz = doc.getroot()
		nivel = raiz[numero_nivel]

		#Decorado y su posicion
		ficheroConversa = nivel.find("ficheroConversa").text
		dec = nivel.find("decorado")
		nombreDecorado = dec.find("nombre").text
		pos = dec.find("posicion")

		posDecorado = (int(pos.get("x")), int(pos.get("y")))
		decorado = dec.find("background").text

		# Posicion del jugador
		#posJ = nivel.find("posJugador")
		#posicionJugador = (int(posJ.get("x")), int(posJ.get("y")))


		# Musica
		music = nivel.find("music").text


		# Fin de Fase
		listaFinFases = []
		finFases = nivel.find("finFases")

		for finFase in finFases.findall("finFase"):
			siguienteFase = finFase.find("siguienteFase").text
			posF = finFase.find("posicion")
			sizeF = finFase.find("size")
			posicionFase = (int(posF.get("x")), int(posF.get("y")))
			sizeFase = (int(sizeF.get("x")), int(sizeF.get("y")))
			listaFinFases.append(rectBloque(siguienteFase, posicionFase, sizeFase))


		# Parseamos Personajes ahora
		listaPersonajes = []
		personajes = nivel.find("personajes")


		for personaje in personajes.findall("personaje"):
			nombre = personaje.find("nombre").text
			posP = personaje.find("posicion")
			posicionPersonaje = (int(posP.get("x")), int(posP.get("y")))
			listaPersonajes.append(Personaje(nombre, posicionPersonaje))



		# Parseamos plataformas ahora

		listaPlataformas = []
		plataformas = nivel.find("plataformas")

		for plataforma in plataformas.findall("plataforma"):
			posPlat = plataforma.find("posicion")
			sizePlat = plataforma.find("size")
			nombre = plataforma.find("nombre").text
			tipo =  plataforma.find("tipo").text
			posicionPlataforma = (int(posPlat.get("x")), int(posPlat.get("y")))
			sizePlataforma = (int(sizePlat.get("x")), int(sizePlat.get("y")))
			listaPlataformas.append(rectBloque(nombre, posicionPlataforma,sizePlataforma, tipo))


		# Parseamos comidas e items curativos
		listaComidas = []
		comidas = nivel.find("comidas")


		for comida in comidas.findall("comida"):
			nombre = comida.find("nombre").text
			tipo = comida.find("tipo").text
			posC = comida.find("posicion")
			posicionComida = (int(posC.get("x")), int(posC.get("y")))
			listaComidas.append(Comidas(nombre, posicionComida,tipo))



		# Finalmente Guardamos el estado del nivel
		if (str(ficheroConversa) != "None"):
			ficheroConversa = str(self.directorioConversaciones) + str(ficheroConversa )
			self.maxTextos,self.listaTextos = self.generarListaTextos (ficheroConversa)
		self.nombreDecorado = nombreDecorado
		self.decorado = decorado
		self.posDecorado = 	posDecorado
		#self.posJugador  = posicionJugador
		self.music = music
		self.listaFinFases = listaFinFases
		self.listaPersonajes = listaPersonajes
		self.listaPlataformas = listaPlataformas
		self.listaComidas = listaComidas


	def generarListaTextos (self,ficheroConversa):
		i=0
		listaTextos = []
		fichero = open(ficheroConversa,'r')
		for line in fichero :
			listaTextos.insert(i, line[0:(len(line)-1)])
			i +=1
		return i,listaTextos

	def leerConfiguracion (self,ficheroProperties):
		self.properties =etree.parse(ficheroProperties)
		raiz = self.properties.getroot()
		propertie = raiz[0]
		self.faseActual = propertie.find("faseActual").text
		self.ficheroImagenes = propertie.find("ficheroImagenes").text
		self.directorioConversaciones = propertie.find("ficheroConversaciones").text

class rectBloque:

	def __init__(self,nombre, posicion, size, tipo = 'null'):
		self.nombre= nombre
		self.tipo = tipo
		self.rect = pygame.Rect(posicion, size)

	def getRect(self):
		return self.rect

	def getNombre (self):
		return self.nombre

	def getTipo (self):
		return self.tipo



class Comidas:
	def __init__(self,nombre, posicion,tipo):
		self.nombre=nombre
		self.posicion = posicion
		self.tipo = tipo


	def getNombre (self):
		return self.nombre

	def getPosX (self):
		return self.posicion[0]

	def getPosY (self):
		return self.posicion[0]

class Personaje:

	def __init__(self,nombre, posicion):
		self.nombre=nombre
		self.posicion = posicion

	def getNombre (self):
		return self.nombre

	def getPosicion(self):
		return posicion
