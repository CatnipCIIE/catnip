# -*- encoding: utf-8 -*-

import pygame
from pygame.locals import *
from gestorRecursos import *
from pantalla import *
from elementogui import *

class PantallaInicial(Pantalla):
    def __init__(self, menu):
        Pantalla.__init__(self, menu, 'portada.png')
        # Creamos los botones y los metemos en la lista (ancho y alto)
        botonMover = BotonMover(self, 'images2.png', (200,210))
        #botonSalir = BotonSalir(self, 'boton_rojo.png', (580,360))

        #iconoJuego = IconoPrincipal(self,'gatico.png', (375,100))
        self.elementosGUI.append(botonMover)
        #self.elementosGUI.append(botonSalir)
        #self.elementosGUI.append(iconoJuego)

        # Creamos el texto y lo metemos en la lista 375
        textoJugar = TextoJugar(self,'Aventura',(290,210))
        textoSalir = TextoSalir(self,'Salir',(290,270))
        self.elementosGUI.append(textoJugar)
        self.elementosGUI.append(textoSalir)

        pygame.mixer.stop()
        self.music = GestorRecursos.load_sound("batman.ogg", 0.1)
        self.music.play(-1)
