#!/usr/bin/env python
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#		It's my first actual game-making attempt. I know code could be much better
#		with classes or defs but I tried to make it short and understandable with very
#		little knowledge of python and pygame(I'm one of them). Enjoy.


from sys import exit
import random
from inputbox import *

import pygame, escena
from escena import *
from personajes import *
from pygame.locals import *
from lxml import etree
from XmlParser import XmlParser

class plataforma:


	def __init__(self,nombre,tipo,paramRect1,paramRect2,paramRect3,paramRect4):
		self.nome=nombre
		self.tipo=tipo
		self.paramRect1 = paramRect1
		self.paramRect2=paramRect2
		self.paramRect3=paramRect3
		self.paramRect4=paramRect4

pygame.init()
ANCHO_PANTALLA = 256
ALTO_PANTALLA = 192
screen=pygame.display.set_mode((ANCHO_PANTALLA, ALTO_PANTALLA))
pygame.display.set_caption("the Fer's Plataform-Engine!")

#Creating 2 bars, a ball and background.


imagen = GestorRecursos.CargarImagen("neil.png" , -1)
##self.dercorado = pygame.transform.scale(self.decorado, (800, 250))
rect = imagen.get_rect()
rect = rect.move(0,0)

posicionx = 0 # El lado izquierdo de la subimagen que se esta visualizando
rectSubimagen = pygame.Rect(0, 0, ANCHO_PANTALLA, ALTO_PANTALLA)
rectSubimagen.left = 0 # El scroll horizontal empieza en la posicion 0 por defecto




#imagen = pygame.transform.scale(imagen, (800, 250))

listaPlataformas =[]
cont =0

imagen.scroll(0,0)
screen.blit(imagen, rect)

ancho=10
alto=10
bar = pygame.Surface((ancho,alto))
bar1 = bar.convert()
bar1.fill((0,0,255))
bar2 = bar.convert()
bar2.fill((255,0,0))
circ_sur = pygame.Surface((15,15))
circ = pygame.draw.circle(circ_sur,(0,255,0),(15/2,15/2),15/2)
circle = circ_sur.convert()
circle.set_colorkey((0,0,0))

# some definitions
bar1_x, bar2_x = 0. , 0.
bar1_y, bar2_y = 0. , 215.
circle_x, circle_y = 307.5, 232.5
bar1_moveX,bar1_moveY, bar2_move = 0. , 0. , 0.
speed_x, speed_y, speed_circ = 50., 50., 50.
bar1_score, bar2_score = 0,0
#clock and font objects
clock = pygame.time.Clock()
font = pygame.font.SysFont("calibri",40)

while True:

    for event in pygame.event.get():
        if event.type == QUIT:
            exit()
        if event.type == KEYDOWN:
            if event.key == K_UP:
                bar1_moveY = -ai_speed
            if event.key == K_DOWN:
            	bar1_moveY = ai_speed
	    if event.key == K_RIGHT:
		bar1_moveX = ai_speed
	    if event.key == K_LEFT:
		bar1_moveX = -ai_speed
	    if event.key == K_x:
		ancho +=10
		bar =pygame.transform.scale(bar,(ancho,alto))
		bar1 = bar.convert()
		bar1.fill((0,0,255))
	    if event.key == K_y:
		alto +=10
		bar =pygame.transform.scale(bar,(ancho,alto))
		bar1 = bar.convert()
		bar1.fill((0,0,255))
	    if event.key == K_c:
		ancho -=10
		bar =pygame.transform.scale(bar,(ancho,alto))
		bar1 = bar.convert()
		bar1.fill((0,0,255))
	    if event.key == K_v:
		alto -=10
		bar =pygame.transform.scale(bar,(ancho,alto))
		bar1 = bar.convert()
		bar1.fill((0,0,255))
	    if event.key == K_SPACE:
		nome = ask(screen, "PlataforName")
		tipo = ask(screen, "tipo")
		platx = plataforma(nome,tipo,bar1_x,bar1_y,bar1.get_width(),bar1.get_height())
		listaPlataformas.insert(cont,platx)
		cont = cont + 1
	    if event.key == K_g:
		print "generando xml..."
		plataformas=etree.Element("plataformas")
		doc=etree.ElementTree(plataformas)


		for i in range (0,cont):
			plataformax = etree.Element("plataforma")
			plataformax.append(etree.Element("nombre"))
			plataformax.append(etree.Element("tipo"))
			for x in range(0,4):
				plataformax.append(etree.Element("paramRect"+str(x+1)))
			plataformax[0].text= listaPlataformas[i].nome
			plataformax[1].text= listaPlataformas[i].tipo
			plataformax[2].text= str(listaPlataformas[i].paramRect1)
			plataformax[3].text= str(listaPlataformas[i].paramRect2)
			plataformax[4].text= str(listaPlataformas[i].paramRect3)
			plataformax[5].text= str(listaPlataformas[i].paramRect4)
	    		plataformas.append(plataformax)
		arbol = etree.ElementTree(plataformas)
		arbol.write("plataformas.xml")


        elif event.type == KEYUP:
            if event.key == K_UP:
                bar1_moveY = 0.
            elif event.key == K_DOWN:
                bar1_moveY = 0.
	    elif event.key == K_RIGHT:
		bar1_moveX = 0.
	    elif event.key == K_LEFT:
		bar1_moveX = 0.


    screen.blit(imagen,(0,0))
    #frame = pygame.draw.rect(screen,(255,255,255),Rect((5,5),(630,470)),2)

    screen.blit(bar1,(bar1_x,bar1_y))

    bar1_x += bar1_moveX
    bar1_y += bar1_moveY


# movement of circle
    time_passed = clock.tick(30)
    time_sec = time_passed / 1000.0

    circle_x += speed_x * time_sec
    circle_y += speed_y * time_sec
    ai_speed = speed_circ * time_sec


    pygame.display.update()
