from personajes import *

class plataforma: 


	def __init__(self,nombre,paramRect1,paramRect2,paramRect3,paramRect4):
		self.nombre=nombre
		self.paramRect1 = paramRect1
		self.paramRect2=paramRect2
		self.paramRect3=paramRect3
		self.paramRect4=paramRect4

	def instaciar (self):
		self.intancia = PlataformaX(pygame.Rect(self.getParamRect1(), self.getParamRect2(),self.getParamRect3(),self.getParamRect4()))

	def getPlataformaInstanciada(self):
		return self.instancia

	def getNombre (self):
		return self.nombre
	
	def getParamRect1(self):
		return self.paramRect1

	def getParamRect2(self):
		return self.paramRect2

	def getParamRect3(self):
		return self.paramRect3

	def getParamRect4(self):
		return self.paramRect4

class PlataformaX(MiSprite):
    def __init__(self,rectangulo):
        # Primero invocamos al constructor de la clase padre
        MiSprite.__init__(self)
        # Rectangulo con las coordenadas en pantalla que ocupara
        self.rect = rectangulo
        # Y lo situamos de forma global en esas coordenadas
        self.establecerPosicion((self.rect.left, self.rect.bottom))
        # En el caso particular de este juego, las plataformas no se van a ver, asi que no se carga ninguna imagen
        self.image = pygame.Surface((0, 0))

	
