\select@language {spanish}
\contentsline {section}{\numberline {1}Desarollo Art\IeC {\'\i }stico}{2}
\contentsline {subsection}{\numberline {1.1}Antecedentes}{2}
\contentsline {subsubsection}{\numberline {1.1.1}Ambientaci\IeC {\'o}n}{2}
\contentsline {subsubsection}{\numberline {1.1.2}Historia}{2}
\contentsline {subsection}{\numberline {1.2}Personajes}{3}
\contentsline {subsection}{\numberline {1.3}Otras caracteristicas de la ambientaci\IeC {\'o}n}{5}
\contentsline {subsubsection}{\numberline {1.3.1}Objetos destacados}{5}
\contentsline {subsubsection}{\numberline {1.3.2}Lugares}{5}
\contentsline {subsection}{\numberline {1.4}Gui\IeC {\'o}n}{6}
\contentsline {subsubsection}{\numberline {1.4.1}Modo historia}{6}
\contentsline {subsubsection}{\numberline {1.4.2}Diferencias 2D con 3D}{6}
\contentsline {section}{\numberline {2}Desarollo T\IeC {\'e}cnico 2D}{7}
\contentsline {subsection}{\numberline {2.1}Descripci\IeC {\'o}n}{7}
\contentsline {subsubsection}{\numberline {2.1.1}Personajes}{8}
\contentsline {subsubsection}{\numberline {2.1.2}Enemigos}{8}
\contentsline {subsubsection}{\numberline {2.1.3}Objetos}{8}
\contentsline {subsubsection}{\numberline {2.1.4}Bloques}{8}
\contentsline {subsection}{\numberline {2.2}Dise\IeC {\~n}o}{9}
\contentsline {subsection}{\numberline {2.3}Escena}{10}
\contentsline {paragraph}{\numberline {2.3.0.1}XML Parser}{10}
\contentsline {paragraph}{\numberline {2.3.0.2}Escena 1: Men\IeC {\'u} Inicial}{11}
\contentsline {paragraph}{\numberline {2.3.0.3}Escena 2: Edificio Pawson}{12}
\contentsline {subparagraph}{Descripci\IeC {\'o}n}{12}
\contentsline {subparagraph}{Modelo}{12}
\contentsline {paragraph}{\numberline {2.3.0.4}Escena 3: Oficina Pawson}{13}
\contentsline {paragraph}{\numberline {2.3.0.5}Escena 4: Calles de MichoTown}{13}
\contentsline {paragraph}{\numberline {2.3.0.6}Escena 5: Conversaci\IeC {\'o}n en Club Gato Mareado}{13}
\contentsline {paragraph}{\numberline {2.3.0.7}Escena 6: Combate en Club Gato Mareado}{13}
\contentsline {paragraph}{\numberline {2.3.0.8}Escena 7: Creditos}{14}
\contentsline {subsubsection}{\numberline {2.3.1}Aspectos Destacables}{14}
\contentsline {subsubsection}{\numberline {2.3.2}Manual de usuario}{14}
